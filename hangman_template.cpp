
#include <iostream>
#include <string>



#ifdef _WIN32

//Windows Stuff

//Precompiled header. Remove, if this gives compiler errors
//#include "stdafx.h"

#include <Winsock2.h>
#include <WS2tcpip.h>

#pragma comment(lib,"ws2_32.lib")

using SOCKET_T = SOCKET;

inline int LastError() {

	return WSAGetLastError();

}

inline void ShutdownSocket(SOCKET_T sock) {

	closesocket(sock);

}


//Ignore this part! 
//Windows needs a global initialization of its network subsystem
//and to make sure its the first thing initialized and the last thing uninitialized
//we use this singleton class.
class NetworkInitialization {
public:

	NetworkInitialization() {
		
		WSADATA wsaData;
		WSAStartup(MAKEWORD(2, 2), &wsaData);
		
	}

	~NetworkInitialization() {
		
		WSACleanup();
		
	}
	
private:

	static NetworkInitialization singleton;
	
};

NetworkInitialization NetworkInitialization::singleton = NetworkInitialization();


#else

//Linux, Apple stuff

#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


using SOCKET_T = int;
constexpr int SOCKET_ERROR = -1;


inline int LastError() {

	return errno;

}

inline void ShutdownSocket(SOCKET_T sock) {

	close(sock);

}

#endif



class Connection {
public:

	explicit Connection(std::string ipAddr, int port) : sock(-1) {
		
		//TODO:
		//use the socket() and connect() function to establish a connection to the server
		//for the given IP on the given port. Use the membervariable sock for this purpose!
		//Hint: You need an IPv4 TCP socket
		
		//Windows:
		//https://msdn.microsoft.com/en-us/library/windows/desktop/ms740506(v=vs.85).aspx
		//https://msdn.microsoft.com/en-us/library/windows/desktop/ms737625(v=vs.85).aspx
		
		//Linux:
		//http://man7.org/linux/man-pages/man2/socket.2.html
		//http://man7.org/linux/man-pages/man2/connect.2.html
		
		
		
	}
	
	//Sends one char (@param1) to the server
	void sendChar(char x) {
		
		//Ignore static_assert if you are not familiar with this concept!
		static_assert(sizeof(x) == 1, "Needs a datatype which is one byte long");
		
		int bytesSend = send(sock, &x, sizeof(x), 0);
		if(bytesSend < 0){
			
			throw std::system_error(std::error_code(LastError(), std::system_category()), "Connection error");
			
		}
		
	}
	
	//Receives @param bytes from the server and returns it as string
	std::string recvString(int bytesToRecv){
		
		std::string str(bytesToRecv, '\0');
		int bytesRead = recv(sock, const_cast<char*>(str.data()), bytesToRecv, 0);
		if(bytesRead < 0){
			
			//Connection error, should never happen
			throw std::system_error(std::error_code(LastError(), std::system_category()), "Connection error");
			
		}
		else if(bytesRead == 0) {
			
			//Connection has been closed from the server
			return std::string(); //return empty string
			
		}
		
		return str;
		
	}
	
	void sendString(std::string str) {
	
		//TODO:
		//this function should send the string given in the parameter to the server
		
		
	}
	
	int recvInt(){
		
		//Ignore static_assert if you are not familiar with this concept!
		static_assert(sizeof(int) == 4, "Needs an integer datatype which is 4 bytes long");
		
		int integer;
		int bytesRead = recv(sock, reinterpret_cast<char*>(&integer), sizeof(integer), 0);
		if(bytesRead < 0){
			
			//Connection error: Should never happen
			throw std::system_error(std::error_code(LastError(), std::system_category()), "Connection error");
			
		}
		else if (bytesRead == 0){
			
			//This means the connection has been closed
			//If you get this result you are probably doing something wrong
			return -1;
			
		}
		
		return integer;
		
	}
	
	explicit operator bool() {
		
		return sock != SOCKET_ERROR;
		
	}
	
	~Connection() {
		
		if(sock != -1) {
			
			ShutdownSocket(sock);
			sock = -1;
			
		}
		
	}
	
private:

	SOCKET_T sock;
	
};



int main(int argc, char** argv) {
		
	//Port is 40596 for Milestone1 and 40597 for Milestone2
	//IP will be announced
	Connection cc = Connection("127.0.0.1", 40596);
	if(static_cast<bool>(cc)) {
		
		//Connection established
		//TODO: Play Hangman with the server
		
		
		
		
		
	}
	else{
		
		//Error
		std::cout << "Failed to establish a connection" << std::endl;
		
	}
	
	return 1;
}





