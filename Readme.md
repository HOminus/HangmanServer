# C++ Challenge

## Ablauf
- Registrierung ab 13:00 Uhr
- Kurzes Willkommen und Vorstellen des MSP-Programm ab 13:25 (Mesut?)
- Microsoft HR (Personalabteilung) würde gerne vorbeikommen und was sagen, nachdem ich nicht wusste wann wir fertig sind habe ich Tobi gesagt er soll sie für 13:30 zu uns einladen. 
- Einführung in C++ ab 13:40 (Felix?)
- Vorstellen der Aufgabe ab 14:10 (Oliver?)
- Beginn der Coding Challenge ab 14:20
- Ab ~17:20 Auswertung der Challenge und Preisverleihung
- Bla Bla Bla


## Aufgabe
- Die Teilnehmer organisieren sich in Teams bestehend aus 3 Personen
- Die Aufgabe sieht zwei Meilensteine vor, die die Teams zu erreichen haben

- Im ersten Meilenstein sollen die Teams einen Clienten programmieren, mit dem sie auf der Konsole Hangman gegen den Server spielen können

- Im zweiten Meilenstein soll das Hangman Spiel automatisiert ablaufen
- Zusätzlich bekommen die Teams für die zweite Version eine eindeutige ID von uns mit der sie sich gegenüber dem Server identifizieren müssen. Dies ist für die Challenge wichtig

## Challenge
- In der Challenge treten die verschiedenen Teams mit ihrer zweiten Version gegen den Server an
- Insgesamt müssen die Programme der Teams 10(?) verschiedene Wörter erraten, wobei alle Teams die gleichen 10 Wörter erraten müssen
- Aufgrund der ID die wir den Teilnehmern gegeben haben, können mit den vom Server gesammelten Daten die besten drei Teams ermitteln


## Preise (Mesut)
- Bereits organisiert








