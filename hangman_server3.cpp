

#include <iostream>
#include <fstream>
#include <vector>
#include <random>
#include <mutex>
#include <thread>
#include <string>
#include <exception>
#include <system_error>
#include <map>
#include <memory>
#include <sstream>

constexpr int MAX_GAMES_IN_ONE_SESSION = 10;
constexpr int MAX_COUNT_TRIES = 15;
constexpr int PORT = 40596;

#ifdef _WIN32

//Windows Stuff

//Precompiled header can be
//removed, if this gives compiler errors
//#include "stdafx.h"

#include <Winsock2.h>
#include <WS2tcpip.h>

#pragma comment(lib,"ws2_32.lib")

using SOCKET_T = SOCKET;

inline int LastError() {

	return WSAGetLastError();

}

inline void ShutdownSocket(SOCKET_T sock) {

	closesocket(sock);

}


#else

//Linux, Apple stuff

#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


using SOCKET_T = int;
constexpr int SOCKET_ERROR = -1;


inline int LastError() {

	return errno;

}

inline void ShutdownSocket(SOCKET_T sock) {

	if(sock != -1)
	close(sock);

}

#endif


class Hangman {
public:

	explicit Hangman(std::string w) : word(std::move(w)), guessedWord(word.size(), '_') {

		counter = 0;
		wordCounter = 0;

	}
	
	Hangman() : Hangman(wordList[0]) {
		
	}
	
	int wordLength() const {

		return word.size();

	}

	void startWithNewWord(){
		
		wordCounter++;
		
		word = wordList[wordCounter];
		guessedWord = std::string(word.size(), '_');
		
		totalGamesPlayed++;
		
		totalWrongGuessCounter += counter;
		
		counter = 0;
		
	}
	
	int getTotalGamesPlayed(){
		
		return totalGamesPlayed;
		
	}
	
	const std::string& getWord() const {

		return word;

	}

	bool isEndOfGame() const {

		return (counter >= MAX_COUNT) || hasWon();

	}

	bool hasWon() const {

		return guessedWord == word;

	}

	void putChar(const char x) {

		if (counter <= MAX_COUNT) {

			bool guessdCharCorrectly = false;
			for (int i = 0; i < word.length(); i++) {
				
				if (word[i] == x) {
					
					guessdCharCorrectly = true;
					guessedWord[i] = x;
					
				}

			}
			
			if(guessdCharCorrectly == false){
				
				counter++;
				
			}
			
		}

	}

	const std::string& getGuessedWord() const {

		return guessedWord;

	}

	int getCount() const {

		return counter;

	}

	int getTotalCount() {
		
		return totalWrongGuessCounter;
		
	}
	
	static void ReadWordList(std::string path) {
		
		wordList.push_back("rindfleischettiketierungsueberwachungsgesetz");
		wordList.push_back("xylophon");
		wordList.push_back("forstwirtschaft");
		wordList.push_back("binnenschifffahrtsamt");
		wordList.push_back("fussgaengerzone");
		wordList.push_back("vogelschutzgebiet");
		wordList.push_back("betriebssystem");
		wordList.push_back("pizza");
		wordList.push_back("holomorph");
		wordList.push_back("stachelbeere");
		
	}
	
private:

	static constexpr int MAX_COUNT = MAX_COUNT_TRIES;
	
	//Theoretisch kann es zu data races kommen, d.h. explizite synchronisierung
	static std::mutex mtx;
	static std::vector<std::string> wordList;

	std::string word;
	std::string guessedWord;

	int counter;
	
	int totalGamesPlayed = 0;
	
	int totalWrongGuessCounter = 0;
	
	int wordCounter = 0;

};

std::mutex Hangman::mtx;
std::vector<std::string> Hangman::wordList;

enum class CONNECTION_STATE {
	
	INITIALIZED,
	PLAYING,
	GAME_OVER,
	START_NEW_GAME
	
};

class ClientConnection {
public:

	explicit ClientConnection(SOCKET_T s) : sock(s), connectionState(CONNECTION_STATE::INITIALIZED) {

	}

	ClientConnection(ClientConnection&) = delete;
	ClientConnection& operator=(ClientConnection&) = delete;
	
	ClientConnection(ClientConnection&& mv){
		
		connectionState = mv.connectionState;
		sock = mv.sock;
		mv.sock = -1;
		
	}
	
	ClientConnection& operator=(ClientConnection&& mv) {
		
		connectionState = mv.connectionState;
		ShutdownSocket(sock);
		sock = mv.sock;
		mv.sock = -1;
		
	}
	
	void run(Hangman& hang) {
		
		switch(connectionState){
		case CONNECTION_STATE::INITIALIZED:
			
			initializedState(hang);
			
			break;
			
		case CONNECTION_STATE::PLAYING:
			
			playState(hang);
			
			break;
			
		case CONNECTION_STATE::GAME_OVER:
		
			gameOverState(hang);
		
			break;
			
		case CONNECTION_STATE::START_NEW_GAME:
		
			connectionState = CONNECTION_STATE::PLAYING;
		
			break;	
			
		}

	}

	std::string recvString(int bytesToRecv){
		
		std::string str(bytesToRecv, '\0');
		int bytesRead = recv(sock, const_cast<char*>(str.data()), bytesToRecv, 0);
		if(bytesRead < 0){
			
			//Connection error, should never happen
			throw std::system_error(std::error_code(LastError(), std::system_category()), "Connection error");
			
		}
		else if(bytesRead == 0){
			
			//Connection has been closed from the server
			return std::string(); //return empty string
			
		}
		
		return str;
		
	}

	~ClientConnection() {

		ShutdownSocket(sock);
		sock = -1;

	}

private:
	
	void sendWordLength(Hangman& hang){
		
		int i = hang.wordLength();
		static_assert(sizeof(i) == 4, "Need a data type with 4 bytes");
		
		if(send(sock, reinterpret_cast<char*>(&i), sizeof(i), 0) < 0)
			throw std::system_error(LastError(), std::system_category());
		
	}

	void initializedState(Hangman& hang){
		
		char x = recvChar();
		if(x != '+') {
			
			throw std::exception(); //Unexpected char
			
		}
		else{
			
			sendWordLength(hang);
			
			connectionState = CONNECTION_STATE::PLAYING;
			
		}
		
	}

	void playState(Hangman& hang){
		
		char x = recvChar();
		if(x >= 'a' && x <= 'z') { //Client guessed a character
			
			hang.putChar(x);
			
			if(hang.isEndOfGame()) {
				
				connectionState = CONNECTION_STATE::GAME_OVER;
				
			}
			else {
				
				sendString(hang.getGuessedWord());
				
			}
			
		}
		else if (x == '+') { //New game
			
			hang.startWithNewWord();// = Hangman();
			sendWordLength(hang);
			
			connectionState = CONNECTION_STATE::PLAYING;
		
		}
		
	}
	
	void gameOverState(Hangman& hang){
		
		if(hang.hasWon()){
			
			sendString(hang.getGuessedWord());
			
		}
		else{
			
			sendString(std::string(hang.wordLength(), '*'));			
			
		}
		
		hang.startWithNewWord();
		connectionState = CONNECTION_STATE::INITIALIZED;
		
	}
	
	char recvChar() {

		char x;
		int bytesRecvd = recv(sock, &x, sizeof(x), 0);
		if (bytesRecvd < 0) {

			//Connection closed or internal error
			throw std::system_error(LastError(), std::system_category(), "Connection error occured"); 

		}
		else if(bytesRecvd == 0){
			
			throw std::exception();
			
		}

		return x;

	}
	
	void sendString(const std::string& str) {

		int bytesSend = send(sock, str.data(), str.size(), 0);
		if (bytesSend < 0)
			throw std::system_error(LastError(), std::system_category(), "Connection error occured");
	
	}
	
	CONNECTION_STATE connectionState;
	SOCKET_T sock;

};

class Game {
public:
	
	static void StartNewGameAsync(SOCKET_T sock){
		
		auto g = Game(sock);
		
		std::string id = g.conn.recvString(10);
		auto it = GameList.find(id);
		if(it != std::end(GameList)) {
			
			if(it->second != nullptr){
				
				std::cout << "TeamID : " << id << "has already played the challenge, no second chance!" << std::endl;
				return;
				
			}
			
			g.currentGameId = std::move(id);
			it->second = std::make_unique<Game>(std::move(g));
			
			std::thread([&game = *(it->second)]{
				
				try{
					
					while(game.hangmanGame.getTotalGamesPlayed() < MAX_GAMES_IN_ONE_SESSION){
						
						game.conn.run(game.hangmanGame);
						
					}
					
				}
				catch(std::system_error& se) {
			
				std::cout << "Error on the connection occured: " << se.what() << std::endl;
				
				}
				catch (std::exception& ex) {
		
					std::cout << "Connection closed by client: " << game.currentGameId << " (This does not indicate a failure)" <<std::endl;
			
				}

				std::stringstream sstr;
				sstr << "The Team with the TeamID: " << game.currentGameId << "has a total amount of " << game.hangmanGame.getTotalCount()
							<< " wrong guesses\n";
				
				std::cout << sstr.str() << std::endl;
				
				game.conn.~ClientConnection();			
			
			}).detach();			
			
		}
		else{
			
			std::cout << "Unexpected ID: " << id << std::endl;
			
		}
		
	}
	
	Game(SOCKET_T s) : currentGameId(), conn(s) {
		
	}
	
	static void InitGameList() {
		
		std::string UniqueKeys[] = {"Team1_aWe4", "Team2_xZu7", "Team3_iHr3", "Team4_tNb8", "Team5_rDq4", "Team6_eRp0", 
								"Team7_zSa7", "Team8_rIa2", "Team9_vGn0", "Team10rTv6", "Team11uEr4", "Team12hTx4",
								"Team13oSd5", "Team14xBl9", "Team15rRq0", "Team16oCs2", "Team17hEm1", "Team20oDf5"};
		
		for(auto& e : UniqueKeys){
			
			GameList[e] = nullptr;
			
		}
		
	}
	
private:

	std::string currentGameId;
	
	Hangman hangmanGame;
	ClientConnection conn;
	
	int totalWrongGuessCounter;
	
	static std::map<std::string, std::unique_ptr<Game>> GameList;
	
};

std::map<std::string, std::unique_ptr<Game>> Game::GameList;

int main(int argc, char** argv) {

#ifdef _WIN32

	//The network subsystem must be initialized explicitly on Windows
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);

#endif

	Hangman::ReadWordList("clean_nouns.txt");

	SOCKET_T listenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (listenSocket == -1) {

		std::cerr << "socket failed\n" << std::endl;
		return -1;

	}

	sockaddr_in sockrecv = { 0 };
	sockrecv.sin_family = AF_INET;
	sockrecv.sin_addr.s_addr = INADDR_ANY;
	sockrecv.sin_port = htons(PORT); //Port is a global variable, it is defined at the very top

	if (bind(listenSocket, reinterpret_cast<sockaddr*>(&sockrecv), sizeof(sockrecv)) == SOCKET_ERROR) {

		std::cerr << "bind failed\n";
		return -1;

	}

	if (listen(listenSocket, SOMAXCONN) == SOCKET_ERROR) {

		std::cerr << "listen\n";
		return -1;

	}

	Game::InitGameList();
	
	int conn;
	while ((conn = accept(listenSocket, nullptr, 0)) != -1) { //This waits for incoming connections

		Game::StartNewGameAsync(conn); //For every new connection a new game is started

	}

	std::cerr << "accept failed\n" << std::endl;

	ShutdownSocket(listenSocket);

#ifdef _WIN32

	WSACleanup();

#endif

}





