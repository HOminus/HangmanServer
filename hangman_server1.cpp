

#include <iostream>
#include <fstream>
#include <vector>
#include <random>
#include <mutex>
#include <thread>
#include <string>
#include <exception>
#include <system_error>

constexpr int MAX_COUNT_TRIES = 15;

constexpr int PORT = 40596;


#ifdef _WIN32

//Windows Stuff

//Precompiled header can be
//removed, if this gives compiler errors
//#include "stdafx.h"

#include <Winsock2.h>
#include <WS2tcpip.h>

#pragma comment(lib,"ws2_32.lib")

using SOCKET_T = SOCKET;

inline int LastError() {

	return WSAGetLastError();

}

inline void ShutdownSocket(SOCKET_T sock) {

	closesocket(sock);

}


#else

//Linux, Apple stuff

#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


using SOCKET_T = int;
constexpr int SOCKET_ERROR = -1;


inline int LastError() {

	return errno;

}

inline void ShutdownSocket(SOCKET_T sock) {

	close(sock);

}

#endif


class Hangman {
public:

	explicit Hangman(std::string w) : word(std::move(w)), guessedWord(word.size(), '_') {

		counter = 0;

	}
	
	Hangman() : Hangman(GetRandomString()) {
		
	}
	
	int wordLength() const {

		return word.size();

	}

	const std::string& getWord() const {

		return word;

	}

	bool isEndOfGame() const {

		return (counter >= MAX_COUNT) || hasWon();

	}

	bool hasWon() const {

		return guessedWord == word;

	}

	void putChar(const char x) {

		if (counter < MAX_COUNT) {

			bool guessdCharCorrectly = false;
			for (int i = 0; i < word.length(); i++) {
				
				if (word[i] == x) {
					
					guessdCharCorrectly = true;
					guessedWord[i] = x;
					
				}

			}
			
			if(guessdCharCorrectly == false){
				
				counter++;
				
			}
			
		}

	}

	const std::string& getGuessedWord() const {

		return guessedWord;

	}

	int getCount() const {

		return counter;

	}

	static void ReadWordList(std::string path) {
		
		std::fstream file(path);
		
		std::string str;
		while(std::getline(file, str)){
			
			wordList.push_back(str);
			
		}
		
	}
	
	static std::string GetRandomString(){
		
		std::unique_lock<std::mutex> lck(mtx);
		
		static std::default_random_engine eng;
		static std::uniform_int_distribution<int> dstr(0, wordList.size() - 1);
		
		return wordList[dstr(eng)];
		
	}
	
private:

	static constexpr int MAX_COUNT = MAX_COUNT_TRIES;
	
	//Theoretisch kann es zu data races kommen, d.h. explizite synchronisierung
	static std::mutex mtx;
	static std::vector<std::string> wordList;

	std::string word;
	std::string guessedWord;

	int counter;

};

std::mutex Hangman::mtx;
std::vector<std::string> Hangman::wordList;

enum class CONNECTION_STATE {
	
	INITIALIZED,
	PLAYING,
	GAME_OVER,
	START_NEW_GAME
	
};

class ClientConnection {
public:

	explicit ClientConnection(SOCKET_T s) : sock(s), connectionState(CONNECTION_STATE::INITIALIZED) {

	}

	void run(Hangman& hang) {
		
		switch(connectionState){
		case CONNECTION_STATE::INITIALIZED:
			
			initializedState(hang);
			
			break;
			
		case CONNECTION_STATE::PLAYING:
		
			playState(hang);
			
			break;
			
		case CONNECTION_STATE::GAME_OVER:
		
			gameOverState(hang);
		
			break;
			
		case CONNECTION_STATE::START_NEW_GAME:
		
			connectionState = CONNECTION_STATE::PLAYING;
		
			break;	
			
		}

	}

	~ClientConnection() {

		ShutdownSocket(sock);

	}

private:

	void sendWordLength(Hangman& hang){
		
		int i = hang.wordLength();
		static_assert(sizeof(i) == 4, "Need a data type with 4 bytes");
		
		if(send(sock, reinterpret_cast<char*>(&i), sizeof(i), 0) < 0)
			throw std::system_error(LastError(), std::system_category());
		
	}

	void initializedState(Hangman& hang){
		
		char x = recvChar();
		if(x != '+') {
			
			throw std::exception(); //Unexpected char
			
		}
		else{
			
			sendWordLength(hang);
			
			connectionState = CONNECTION_STATE::PLAYING;
			
		}
		
	}

	void playState(Hangman& hang){
		
		char x = recvChar();
		if(x >= 'a' && x <= 'z') { //Client guessed a character
			
			hang.putChar(x);
			
			if(hang.isEndOfGame()) {
				
				connectionState = CONNECTION_STATE::GAME_OVER;
				
			}
			else {
				
				sendString(hang.getGuessedWord());
				
			}
			
		}
		else if (x == '+') { //New game
			
			hang = Hangman();
			sendWordLength(hang);
			
			connectionState = CONNECTION_STATE::PLAYING;
			
		}
		
	}
	
	void gameOverState(Hangman& hang){
		
		if(hang.hasWon()){
			
			sendString(hang.getGuessedWord());
			
		}
		else{
			
			sendString(std::string(hang.wordLength(), '*'));			
			
		}
		
		hang = Hangman();
		connectionState = CONNECTION_STATE::INITIALIZED;
		
	}
	
	char recvChar() {

		char x;
		int bytesRecvd = recv(sock, &x, sizeof(x), 0);
		if (bytesRecvd < 0) {

			//Connection closed or internal error
			throw std::system_error(LastError(), std::system_category(), "Connection error occured"); 

		}
		else if(bytesRecvd == 0){
			
			throw std::exception();
			
		}

		return x;

	}

	void sendString(const std::string& str) {

		int bytesSend = send(sock, str.data(), str.size(), 0);
		if (bytesSend < 0)
			throw std::system_error(LastError(), std::system_category(), "Connection error occured");
		
	}

	CONNECTION_STATE connectionState;
	SOCKET_T sock;

};


void StartGameAsync(SOCKET_T sock) {

	std::thread([=] {

		ClientConnection cc(sock);
		
		Hangman hang; //Change to random word
		
		try {

			while(true) {
		
				cc.run(hang);
			
			}

		}
		catch(std::system_error& se) {
			
			std::cout << "Error on the connection ocured" << std::endl;
			
		}
		catch (std::exception& ex) {
	
			std::cout << "Connection closed by client (This does not indicate a failure)" <<std::endl;
		
		}

		//gg

	}).detach();

}


int main(int argc, char** argv) {

#ifdef _WIN32

	//The network subsystem must be initialized explicitly on Windows
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);

#endif

	Hangman::ReadWordList("clean_nouns.txt");

	SOCKET_T listenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (listenSocket == -1) {

		std::cerr << "socket failed\n" << std::endl;
		return -1;

	}

	sockaddr_in sockrecv = { 0 };
	sockrecv.sin_family = AF_INET;
	sockrecv.sin_addr.s_addr = INADDR_ANY;
	sockrecv.sin_port = htons(PORT); //Port is a global variable, it is defined at the very top

	if (bind(listenSocket, reinterpret_cast<sockaddr*>(&sockrecv), sizeof(sockrecv)) == SOCKET_ERROR) {

		std::cerr << "bind failed\n";
		return -1;

	}

	if (listen(listenSocket, SOMAXCONN) == SOCKET_ERROR) {

		std::cerr << "listen\n";
		return -1;

	}

	int conn;
	while ((conn = accept(listenSocket, nullptr, 0)) != -1) { //This waits for incoming connections

		StartGameAsync(conn); //For every new connection a new game is started

	}

	std::cerr << "accept failed\n" << std::endl;

	ShutdownSocket(listenSocket);

#ifdef _WIN32

	WSACleanup();

#endif

}





