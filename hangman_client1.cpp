
#include <iostream>
#include <string>



#ifdef _WIN32

//Windows Stuff

//Precompiled header. Remove, if this gives compiler errors
//#include "stdafx.h"

#include <Winsock2.h>
#include <WS2tcpip.h>

#pragma comment(lib,"ws2_32.lib")

using SOCKET_T = SOCKET;

inline int LastError() {

	return WSAGetLastError();

}

inline void ShutdownSocket(SOCKET_T sock) {

	closesocket(sock);

}


//Ignore this part! 
//Windows needs a global initialization of its network subsystem
//and to make sure its the first thing initialized and the last thing uninitialized
//we use this class.
class NetworkInitialization {
public:

	NetworkInitialization() {
		
		WSADATA wsaData;
		WSAStartup(MAKEWORD(2, 2), &wsaData);
		
	}

	
	~NetworkInitialization() {
		
		WSACleanup();
		
	}
	
private:

	static NetworkInitialization singleton;
	
};

NetworkInitialization NetworkInitialization::singleton = NetworkInitialization();


#else

//Linux, Apple stuff

#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


using SOCKET_T = int;
constexpr int SOCKET_ERROR = -1;


inline int LastError() {

	return errno;

}

inline void ShutdownSocket(SOCKET_T sock) {

	close(sock);

}

#endif



class Connection {
public:

	explicit Connection(std::string ipAddr, int port) : sock(-1) {
		
		sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if(sock == SOCKET_ERROR){
			
			std::cout << "socket() failed\n" << std::endl;		
			
		}
		
		in_addr	inAddr;
		inet_pton(AF_INET, ipAddr.c_str(), &inAddr);
		
		sockaddr_in sockrecv = { 0 };
		sockrecv.sin_family = AF_INET;
		sockrecv.sin_addr = inAddr;
		sockrecv.sin_port = htons(port);
		
		if(connect(sock, reinterpret_cast<sockaddr*>(&sockrecv), sizeof(sockrecv)) == SOCKET_ERROR){
			
			std::cout << "connect() failed\n" << std::endl;
			
		}
		
	}
	
	void sendChar(char x) {
		
		static_assert(sizeof(x) == 1, "Needs a datatype which is one byte long");
		
		int bytesSend = send(sock, &x, sizeof(x), 0);
		if(bytesSend < 0){
			
			throw std::system_error(std::error_code(LastError(), std::system_category()), "Connection error");
			
		}
		
	}
	
	//Receives @param bytes from the server and returns it as string
	std::string recvString(int bytesToRecv){
		
		std::string str(bytesToRecv, '\0');
		int bytesRead = recv(sock, const_cast<char*>(str.data()), bytesToRecv, 0);
		if(bytesRead < 0){
			
			//Connection error, should never happen
			throw std::system_error(std::error_code(LastError(), std::system_category()), "Connection error");
			
		}
		else if(bytesRead == 0){
			
			//Connection has been closed from the server
			return std::string(); //return empty string
			
		}
		
		return str;
		
	}
	
	void sendString(std::string str) {
		
		int bytesSend = send(sock, const_cast<char*>(str.data()), str.size(), 0);
		if(bytesSend < 0){
			
			throw std::system_error(std::error_code(LastError(), std::system_category()), "Connection error");
			
		}
		
	}
	
	int recvInt(){
		
		static_assert(sizeof(int) == 4, "Needs an integer datatype which is 4 bytes long");
		
		int integer;
		int bytesRead = recv(sock, reinterpret_cast<char*>(&integer), sizeof(integer), 0);
		if(bytesRead < 0){
			
			//Connection error: Should never happen
			throw std::system_error(std::error_code(LastError(), std::system_category()), "Connection error");
			
		}
		else if (bytesRead == 0){
			
			//This means the connection has been closed
			//If you get this result you are probably doing something wrong
			return -1;
			
		}
		
		return integer;
		
	}
	
	explicit operator bool() {
		
		return sock != SOCKET_ERROR;
		
	}
	
	~Connection() {
		
		if(sock != -1) {
			
			ShutdownSocket(sock);
			sock = -1;
			
		}
		
	}
	
private:

	SOCKET_T sock;
	
};


class HangmanGame{
public:
	
	HangmanGame(int wordlength) : wordLength(wordlength), guessedWordSoFar(wordlength, '_') {
		
	}
	
	void updateWord(std::string updatedWord){
		
		if(updatedWord[0] == '*'){
			
			//Lost the game
			play = false;
			wrongGuessesCounter++;
			return;
			
		}
		
		if(guessedWordSoFar == updatedWord) {
			
			wrongGuessesCounter++;
			
		}
		else{
			
			guessedWordSoFar = updatedWord;
			
		}
		
		if(hasWon()){
			
			play = false;
			
		}
		
	}
	
	std::string getGuessedWord() {
		
		return guessedWordSoFar;
		
	}
	
	bool keepPlaying() {
		
		return play;
		
	}
	
	bool hasWon(){
		
		return (guessedWordSoFar.find('_') == std::string::npos); //True if no '_' is contained in the string
		
	}
	
	int getWrongGuessCount() {
		
		return wrongGuessesCounter;
		
	}
	
	int getWordLength(){
		
		return wordLength;
		
	}
	
private:

	std::string guessedWordSoFar;
	int wordLength = 0;
	
	int wrongGuessesCounter = 0;
	
	bool play = true;
	
};



int main(int argc, char** argv) {
	
	
	Connection cc = Connection("127.0.0.1", 40596);
	if(static_cast<bool>(cc)) {
		
		char playagain = 'n';
		do{
		
			//Start a new game:
			cc.sendChar('+');
			
			int length = cc.recvInt();
			HangmanGame hg = HangmanGame(length);
			
			std::cout << "The word you have to guess has: " << length << " letters" << std::endl;
			
			do {
				
				std::cout << "Input the letter you want to guess: ";
				char x;
				std::cin >> x;
				
				if(x >= 'a' && x <= 'z'){
					
					cc.sendChar(x);
					
					std::string str = cc.recvString(hg.getWordLength());
					if(str == "") {
						//Connection closed by the server, should never happen
						return -1;
						
					}
					hg.updateWord(str);
					
				}
				
				std::cout << "The word guessed so far is: " << hg.getGuessedWord() << std::endl;
				std::cout << "You had " << hg.getWrongGuessCount() << " wrong guesses so far." << std::endl;
				
			}while(hg.keepPlaying());
			
			
			if(hg.hasWon()){
				
				std::cout << "You won the game after " << hg.getWrongGuessCount() << " wrong guesses. The hidden word was: " << hg.getGuessedWord() << std::endl;
				
			}
			else{
				
				std::cout << "You failed to guess the word within the allowed range of guesses. You had " << hg.getWrongGuessCount() << " wrong guesses. " << std::endl;
			
			}
			
			std::cout << "Do you want to play again? (y/n): " << std::flush;
			std::cin >> playagain;
		
		}while(playagain == 'y');
		
	}
	else{
		
		std::cout << "Failed to establish a connection" << std::endl;
		
	}
	
	return 1;
}





